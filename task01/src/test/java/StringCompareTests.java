import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class StringCompareTests {

    @Test
    @DisplayName("getString test")
    void stringCompare() {
        ApplicationSettings applicationSettings = new ApplicationSettings();
        String result = applicationSettings.getString();
        assertEquals("Application", result);
    }

//    @Test
//    @DisplayName("getString test 2")
//    void stringCompare2() {
//        ApplicationSettings applicationSettings = new ApplicationSettings();
//        String result = applicationSettings.getString();
//        assertEquals("Application2", result);
//    }
}
